﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frequencySort
{
  class Program
  {
    static List<int> FrequencySort(List<int> nums) {
      Dictionary<int, int> map = new Dictionary<int, int>();
      foreach(int num in nums) {
        if(map.ContainsKey(num)) {
          map[num] = map[num] + 1;
        } else {
          map.Add(num, 1);
        }
      }
      map = map.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
      return map.Keys.ToList();
    }

    static void Main(string[] args) {
      List<int> nums = new List<int> { 1, 2, 1, 2, 1, 3, 4, 3, 4, 2, 4, 4, 4, 4  };
      FrequencySort(nums).ForEach(Console.WriteLine);
      Console.ReadLine();
    }
  }
}
